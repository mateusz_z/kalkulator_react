import React from 'react';
import {Text,StyleSheet,TouchableOpacity } from 'react-native';

const Tile = (props) => {
    const type = '';
    const setTileType = (type) => {
        setTileType(type)
    }
    
    return (
        
        <TouchableOpacity style={[styles.viewStyle, {backgroundColor:props.color}]}
         onPress={() => props.onPressButton(props.buttonValue)}
      >
        <Text>{props.buttonValue}</Text>
    </TouchableOpacity>
        
    );
}
const styles = StyleSheet.create({
    viewStyle: {
        borderColor: 'grey',
        borderWidth: 1,
        alignContent: "stretch",
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'

    },
});
export default Tile;