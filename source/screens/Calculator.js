import React, { useState } from 'react';
import { View, Text, StyleSheet,  } from 'react-native';
import Tile from '../components/Tile';
const Calculator = () => {
    const [operationText, setOperationText] = useState('');
    const buttonPress = (buttonValue) => {
        switch (buttonValue) {

            case '=':
            if (operationText.slice(-1) == '%'){
                setOperationText(operationText.split('%')[0]*0.01)
            }else{
            if (operationText.indexOf('=') > -1){}else{
            if (operationText.length == 0) { } else {
                setOperationText(operationText +  buttonValue + eval(operationText));}}}
                break;

            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                if (operationText.indexOf('=') > -1){
                    setOperationText([]+buttonValue);
                }else{
                setOperationText(operationText + buttonValue);}
                break;

            case '*':
            case '/':
            case '-':
            case '+':
            case '%':
                if (operationText.length == 0) { } else {
                    if ((operationText[operationText.length - 1] == '*') ||
                        (operationText[operationText.length - 1] == '/') ||
                        (operationText[operationText.length - 1] == '-') ||
                        (operationText[operationText.length - 1] == '+') ||
                        (operationText[operationText.length - 1] == '%')) {
                        setOperationText(operationText.substring(0, operationText.length - 1) + buttonValue);
                    } else {
                        if (operationText.indexOf('=') > -1){
                            
                            setOperationText( operationText.split('=')[1]+buttonValue);
                        }else{
                        setOperationText(operationText + buttonValue);}
                    }
                }
                break;

            case 'C':
                setOperationText([]);
                break;
            case 'Del':
                if (operationText.length == 0) {

                } else {
                    setOperationText(operationText.substring(0, operationText.length - 1));
                }
                break;
            case '.':
                if ((operationText.indexOf('0.') > -1)) { } else {
                    if ((operationText.indexOf('.') > -1) || (operationText.length == 0)||
                        (operationText[operationText.length - 1] == '*') ||
                        (operationText[operationText.length - 1] == '/') ||
                        (operationText[operationText.length - 1] == '-') ||
                        (operationText[operationText.length - 1] == '+') ||
                        (operationText[operationText.length - 1] == '%')) {
                        setOperationText(operationText + '0' + buttonValue);
                    } else { setOperationText(operationText + buttonValue); }
                }
                break;
        }
    }

    return (
        <View style={styles.fullStyle}>
             <Text
                style={styles.viewStyle}>{operationText}</Text>
            <View style={styles.padStyle}>
                <Tile buttonValue={'C'} onPressButton={buttonPress} color={'pink'}></Tile>
                <Tile buttonValue={'Del'} onPressButton={buttonPress} color={'pink'}></Tile>
                <Tile buttonValue={'%'} onPressButton={buttonPress} color={'orange'}></Tile>
                <Tile buttonValue={'/'} onPressButton={buttonPress} color={'orange'}></Tile>
            </View>
            <View style={styles.padStyle}>
                <Tile buttonValue={'7'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'8'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'9'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'*'} onPressButton={buttonPress} color={'orange'}></Tile>
            </View>
            <View style={styles.padStyle}>
                <Tile buttonValue={'4'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'5'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'6'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'-'} onPressButton={buttonPress} color={'orange'}></Tile>
            </View>
            <View style={styles.padStyle}>
                <Tile buttonValue={'1'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'2'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'3'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'+'} onPressButton={buttonPress} color={'orange'}></Tile>
            </View>
            <View style={styles.padStyle}>
                <Tile buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'0'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'.'} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile buttonValue={'='} onPressButton={buttonPress} color={'red'}></Tile>
            </View>
        </View>

    );
}


const styles = StyleSheet.create({
    viewStyle: {
        borderColor: 'grey',
        borderWidth: 1,
        alignContent: "stretch",
        alignItems: 'stretch',
        flex: 1,
        textAlign: 'center',
        fontSize: 20,

    },
    padStyle: {
        flex: 1,
        flexDirection: 'row',
        borderColor: 'grey',
        borderWidth: 1,
        alignContent: "stretch",
        alignItems: 'stretch',
        width: '100%',
        height: '15%',
        fontSize: 20
    },
    fullStyle: {
        flex: 1,
    },
    equalStyle: {
        backgroundColor: 'orange',
    },
});
export default Calculator;