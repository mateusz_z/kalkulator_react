import Calculator from './source/screens/Calculator';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';


const AppNavigator = createStackNavigator({
  Calculator: {screen: Calculator},
},
{
  initialRouteName: 'Calculator',
  defaultNavigationOptions: {title:'Calc'}
});

export default createAppContainer(AppNavigator);